# Recipe finder

Just an idea for now.
I'd like a tool (maybe a TUI to learn something new) which i can tell about ingredients i got lying around. It responds with questions about what kind of dish i'd wanna make like dessert, snack, 3 course menue for hundred people and then go find some recipes on any public api. Next, it asks if i have further ingredients i'd need for a recipe and consecutively strikes all these, i do not have the ingredients for and repeats until i either wanna go shopping or find a recipe i have all the stuff for. Preferably common ingredients should be asked first. Like eggs, dough, milk, onions and such so you don't first have to skip a hundred exotic ingredients you are very unlikely to own.

Further improvments could be:
- caching
- bookmarking recipes
- adding more public api's to the mix
- adding a skill level config
